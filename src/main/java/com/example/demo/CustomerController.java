package com.example.demo;

import net.logstash.logback.argument.StructuredArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import java.io.IOException;

import static org.slf4j.LoggerFactory.*;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @Value("${param.isFrontEnd}")
    private boolean isFrontEnd;

    private static Logger log = getLogger(CustomerController.class);


    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    Customer sayHello(@RequestParam(value = "id", required = false, defaultValue = "Stranger") String id) {
        Customer customer;
        if (isFrontEnd) {
            log.debug("is frontend");
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Customer> re = restTemplate.getForEntity("http://localhost:8080/customers?id=" + id, Customer.class);
            customer = re.getBody();
        } else {
            customer = customerRepository.findOne(Long.valueOf(id));
            log.debug("is not frontend");
        }
        if (customer == null) {
            throw new NotFoundException("Customer Not Found");
        }
        log.debug("{}; {} {}",
                StructuredArguments.v("action", "customer_found"),
                StructuredArguments.kv("first_name", customer.getFirstName()),
                StructuredArguments.kv("last_name", customer.getLastName()));
        return customer;
    }

    @GetMapping(path = "all")
    public @ResponseBody
    Iterable<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @ExceptionHandler
    @ResponseBody
    ErrorInfo handleWebApplicationException(WebApplicationException exc, HttpServletResponse response) throws IOException {
        response.setStatus(exc.getResponse().getStatus());
        return new ErrorInfo(exc.getResponse().getStatus(), exc.getMessage());
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }


    public static class ErrorInfo {
        public int statusCode;
        public String message;

        public ErrorInfo(int statusCode, String message) {
            this.statusCode = statusCode;
            this.message = message;
        }
    }

}
