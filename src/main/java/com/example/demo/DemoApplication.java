package com.example.demo;

import net.logstash.logback.argument.StructuredArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    private static Logger log = LoggerFactory.getLogger(DemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(CustomerRepository repository) {
        return (args) -> {
            Iterable<Customer> customers = repository.findAll();
            if (!customers.iterator().hasNext()) {
                // save a couple of customers
                repository.save(new Customer("Jack", "Bauer"));
                log.info("{}; {} {}",
                        StructuredArguments.v("action", "customer_saved"),
                        StructuredArguments.kv("first_name", "Jack"),
                        StructuredArguments.kv("last_name", "Bauer"));
                repository.save(new Customer("Chloe", "O'Brian"));
                repository.save(new Customer("Kim", "Bauer"));
                repository.save(new Customer("David", "Palmer"));
                repository.save(new Customer("Michelle", "Dessler"));
            }

            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Customer customer : repository.findAll()) {
                log.info(customer.toString());
            }

            // fetch customers by last name
            for (Customer bauer : repository.findByLastName("Bauer")) {
                log.info("{}; {} {}",
                        StructuredArguments.v("action", "customer_found"),
                        StructuredArguments.kv("first_name", bauer.getFirstName()),
                        StructuredArguments.kv("last_name", bauer.getLastName()));
            }
            log.info("");
        };
    }

}
